package com.example.financialappview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.financialappview.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    val marginBalance = 3948
    val marginPercentage = 23
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.textMarginBalance.text = "$marginBalance - $marginPercentage%"
    }

    fun printNama(){}

    fun printChanges(){}

    fun printAge(){}
}